## Troubleshooting Steps

If you are unable to connect to your EC2 instance, follow these steps to verify the routing table configuration:

1. **Check Route Table Association**:
   Verify that your subnet is associated with a route table.
   ```sh
   aws ec2 describe-route-tables --filters "Name=association.subnet-id,Values=<subnet-id>"
   ```
   If you see an empty result like this:
   ```json
   {
       "RouteTables": []
   }
   ```
   It means your subnet is not associated with any route table.

2. **Identify the Correct Route Table**:
   List all route tables in your VPC to find the appropriate one.
   ```sh
   aws ec2 describe-route-tables --filters "Name=vpc-id,Values=<vpc-id>"
   ```

3. **Check the Route to the Internet Gateway**:
   Ensure the route table has a route to the Internet Gateway. Look for an entry with `DestinationCidrBlock` as `0.0.0.0/0` and `GatewayId` as `igw-xxxxxx`.
   ```sh
   aws ec2 describe-route-tables --route-table-ids <rtb-id>
   ```
   Example output:
   ```json
   {
       "RouteTables": [
           {
               "RouteTableId": "rtb-041376b43ab3226b1",
               "Routes": [
                   {
                       "DestinationCidrBlock": "10.0.0.0/16",
                       "GatewayId": "local",
                       "State": "active"
                   },
                   {
                       "DestinationCidrBlock": "0.0.0.0/0",
                       "GatewayId": "igw-0c7e567dc7e2fbcd9",
                       "State": "active"
                   }
               ]
           }
       ]
   }
   ```

4. **Associate the Route Table with the Subnet**:
   If the subnet is not associated with a route table, associate it with the appropriate route table.
   ```sh
   aws ec2 associate-route-table --subnet-id <subnet-id> --route-table-id <rtb-id>
   ```