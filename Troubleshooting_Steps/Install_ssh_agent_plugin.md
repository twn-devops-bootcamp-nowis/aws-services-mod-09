### Install SSH Agent Plugin and Create SSH Credentials

1. Go to Jenkins, navigate to **Manage Jenkins** > **Manage Plugins**, search for **SSH Agent** in the available plugins, and install it without restart.

2. Create SSH credentials:
   - On your local computer, connect to the EC2 instance using SSH to obtain the private key.
   - Make the private key available in Jenkins.
   - Create a new `aws-multibranch-pipeline` for this module with the current project.

3. In Jenkins, navigate to **Dashboard** > `aws-multibranch-pipeline` > **Credentials** > **Folder** > **Global credentials (unrestricted)**.
   - Change the **Kind** to **SSH Username with private key**.
   - Name it `ec2-server-key`.
   - Set the username to `ec2-user`.
   - Paste the contents of the `WebServerKeyPair.pem` into the key portion and click **Create**.