# Setting Up GitLab Integration in Jenkins

## 1. Add GitLab Plugin to Jenkins
1. Navigate to **Manage Jenkins** > **Manage Plugins**.
2. In the **Available** tab, search for **GitLab Plugin**.
3. Check the box next to **GitLab Plugin** and click **Install without restart**.

## 2. Configure GitLab Connection in Jenkins
1. Go to **Manage Jenkins** > **Configure System**.
2. Scroll down to the **GitLab** section and click **Add GitLab Server**.
3. Enter a name for the connection : `gitlab-conn`
4. Enter the GitLab host URL: `https://gitlab.com/`.
5. For **Credentials**, click **Add** > **Jenkins** and select **GitLab API token**.

## 3. Obtain GitLab API Token
1. Log in to your GitLab account.
2. Navigate to **User Settings** > **Access Tokens**.
3. Create a new token and name it `jenkins`.
4. Select the scope `api`.
5. Click **Create personal access token**.
6. Copy the generated token.

## 4. Add GitLab API Token to Jenkins
1. Go back to Jenkins.
2. In the **Add GitLab Server** section, paste the copied token in the **API token** field.
3. Name the token `GitLab-token`.
4. Click **Test Connection** to verify the connection.
5. Click **Save**.

## 5. Configure Build Triggers in Jenkins
1. In your Jenkins pipeline configuration, go to the **Build Triggers** section.
2. Check the box for **Build when a change is pushed to GitLab. GitLab webhook URL: <URL>**.
3. Enable triggers for **Push Events** and **Opened Merge Request Events**.

## 6. Configure GitLab Integration
1. Go to your GitLab project.
2. Navigate to **Settings** > **Integrations**.
3. Find **Jenkins** and enable the integration.
4. In the **URL** field, enter your Jenkins server URL.
5. If a secure connection is required, enable the secure connection option.
6. Enter the project name exactly as it appears in Jenkins.
7. Use the same username and password as your Jenkins credentials.
8. Click **Save Changes**.

## 7. Verify Integration
1. Go back to Jenkins and test the settings.
2. Ensure that the connection between GitLab and Jenkins is successfully established.
