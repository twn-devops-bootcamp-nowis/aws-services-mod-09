

<details>
  <summary>Exercise 1: Create IAM user</summary>
  
  ### Permissions needed for the AWS user
  - Create VPC and Subnet
  - Create EC2 instance
  - Create Security Group for EC2

  ### Steps:
  1. **Check AWS admin user configuration locally**
     ```sh
     aws configure list
     cat ~/.aws/credentials
     ```

  2. **Create a new IAM user using "your name" with UI and CLI access**
     ```sh
     aws iam create-user --user-name <your-name>
     ```

  3. **Create a group "devops"**
     ```sh
     aws iam create-group --group-name devops
     ```

  4. **Add your user to the "devops" group**
     ```sh
     aws iam add-user-to-group --user-name <your-name> --group-name devops
     ```

  5. **Verify that the devops group contains your user**
     ```sh
     aws iam get-group --group-name devops
     ```

  6. **Generate user keys for CLI access & save key.txt file in a safe location**
     ```sh
     aws iam create-access-key --user-name <your-name> > key.txt
     ```

  7. **Generate user login credentials for UI & save password in a safe location**
     ```sh
     aws iam create-login-profile --user-name <your-name> --password <your-password> --password-reset-required
     ```

  8. **Give user permission to change password**
     ```sh
     aws iam list-policies | grep ChangePassword
     aws iam attach-user-policy --user-name <your-name> --policy-arn "arn:aws:iam::aws:policy/IAMUserChangePassword"
     ```

  9. **Check which policies are available for managing EC2 & VPC services, including subnet and security groups**
     ```sh
     aws iam list-policies | grep EC2FullAccess
     aws iam list-policies | grep VPCFullAccess
     ```

  10. **Give devops group needed permissions**
      ```sh
      aws iam attach-group-policy --group-name devops --policy-arn "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
      aws iam attach-group-policy --group-name devops --policy-arn "arn:aws:iam::aws:policy/AmazonVPCFullAccess"
      ```

  11. **Check policies for group**
      ```sh
      aws iam list-attached-group-policies --group-name devops
      ```

  ### Screenshot:
  ![Screenshot](./images/Mod9_Exercise1.png)

</details>

<details>
  <summary>Exercise 2: Configure AWS CLI</summary>
  
  ### Steps:
  1. **Save your current admin user keys from ~/.aws/credentials in a safe location**

  2. **Set credentials for the new user in AWS CLI from key.txt file**
     ```sh
     aws configure
     # Enter new access key ID and secret access key
     ```

  3. **Configure correct region for your AWS CLI**
     ```sh
     aws configure
     # Enter the default region name and output format
     ```

  4. **Validate that ~/.aws/credentials contains the keys of the new user**
     ```sh
     cat ~/.aws/credentials
     ```

  ### Screenshot:
  ![Screenshot](./images/Mod9_Exercise2.png)
  ![aws sts get-caller-identity](./images/Mod9_Exercise2-1.png)

</details>

<details>
  <summary>Exercise 3: Create VPC</summary>
  
  ### Create VPC with 1 Subnet:
  Refer to the official [AWS CLI VPC documentation](https://docs.aws.amazon.com/vpc/latest/userguide/vpc-subnets-commands-example.html) for the latest commands.

  1. **Create VPC and return VPC ID**
     ```sh
     aws ec2 create-vpc --cidr-block 10.0.0.0/16 --query Vpc.VpcId --output text
     ```

  2. **Create subnet in the VPC**
     ```sh
     aws ec2 create-subnet --vpc-id <vpc-id> --cidr-block 10.0.1.0/24
     ```

  3. **Return subnet ID**
     ```sh
     aws ec2 describe-subnets --filters "Name=vpc-id,Values=<vpc-id>"
     ```

  ### Make subnet public by attaching it to an internet gateway:

  4. **Create internet gateway & return the gateway ID**
     ```sh
     aws ec2 create-internet-gateway --query InternetGateway.InternetGatewayId --output text
     ```

  5. **Attach internet gateway to the VPC**
     ```sh
     aws ec2 attach-internet-gateway --vpc-id <vpc-id> --internet-gateway-id <igw-id>
     ```

  6. **Create a custom Route table for the VPC & return route table ID**
     ```sh
     aws ec2 create-route-table --vpc-id <vpc-id> --query RouteTable.RouteTableId --output text
     ```

  7. **Create Route rule for handling all traffic between internet & VPC**
     ```sh
     aws ec2 create-route --route-table-id <rtb-id> --destination-cidr-block 0.0.0.0/0 --gateway-id <igw-id>
     ```

  8. **Validate the custom route table has correct configuration**
     ```sh
     aws ec2 describe-route-tables --route-table-id <rtb-id>
     ```

  9. **Associate subnet with the route table to allow internet traffic in the subnet**
     ```sh
     aws ec2 associate-route-table --subnet-id <subnet-id> --route-table-id <rtb-id>
     ```

  ### Create security group in the VPC to allow access on port 22:

  10. **Create security group and get the security group ID**
      ```sh
      aws ec2 create-security-group --group-name SSHAccess --description "Security group for SSH access" --vpc-id <vpc-id>
      ```

  11. **Add incoming access on port 22 from all sources to the security group**
      ```sh
      aws ec2 authorize-security-group-ingress --group-id <sg-id> --protocol tcp --port 22 --cidr 0.0.0.0/0
      ```

  ### Screenshot:
  ![Screenshot](./images/Mod9_Exercise3.png)

</details>

<details>
  <summary>Exercise 4: Create EC2 Instance</summary>
  
  ### Create EC2 instance in the subnet:

  1. **Create key pair, save it locally in a pem file and set stricter permissions on it for later use**
     ```sh
     aws ec2 create-key-pair --key-name WebServerKeyPair --query "KeyMaterial" --output text > WebServerKeyPair.pem
     chmod 400 WebServerKeyPair.pem
     ```

  2. **Create EC2 instance with the key, in the subnet, and using the security group**
     ```sh
     aws ec2 run-instances --image-id <image-id> --count 1 --instance-type t2.micro --key-name WebServerKeyPair --security-group-ids <sg-id> --subnet-id <subnet-id> --associate-public-ip-address
     ```

  3. **Validate that the EC2 instance is in a running state and get its public IP address**
     ```sh
     aws ec2 describe-instances --instance-id <instance-id> --query "Reservations[*].Instances[*].{State:State.Name,Address:PublicIpAddress}"
     ```

  ### Screenshot:
  ![Screenshot](./images/Mod9_Exercise4.1.png)

</details>

<details>
  <summary>Exercise 5: SSH into the server and install Docker on it</summary>
  
  ### Steps:

  1. **SSH into EC2 instance using the public IP address**
     ```sh
     ssh -i "WebServerKeyPair.pem" ec2-user@<public-ip-address>
     ```

  2. **Install Docker, start Docker service, and allow ec2-user to run Docker commands without sudo**
     ```
     sudo yum update -y && sudo yum install -y docker
     sudo systemctl start docker
     sudo usermod -aG docker ec2-user
     ```

  ### Screenshot:
  ![Screenshot](./images/M0d9_Exercise5.png)

</details>

<details>
  <summary>Exercise 6: Add docker-compose for deployment</summary>
  
  ### docker-compose.yaml:
  ```yaml
  version: '3.8'
  services:
    nodejs-app:
      image: ${IMAGE}
      ports:
        - "3000:3000"
  ```

  ### Example:
[Location of docker compose file](./docker-compose.yaml)

</details>

<details>
  <summary>Exercise 7: Add "deploy to EC2" step to your pipeline</summary>
  
  ### Steps:

  1. **Create SSH keys credentials in Jenkins**
     - Name the keys credentials: `ec2-server-key` and add the private SSH key from `WebServerKeyPair.pem` as its content.
    [Use the following to add credentials](./Troubleshooting_Steps/Install_ssh_agent_plugin.md)

  2. **Create server-cmds.sh file**
     ```sh
     export IMAGE=$1
     docker-compose -f docker-compose.yaml up --detach
     echo "success"
     ```

  3. **Add the 'deploy to EC2' step to the Jenkinsfile**
```groovy
#!/usr/bin/env groovy

library identifier: 'jenkins-shared-library@master', retriever: modernSCM(
    [$class: 'GitSCMSource',
    remote: 'https://gitlab.com/twn-devops-bootcamp-nowis/exercises/jenkins-shared-library-mod-9.git',
    credentialsID: 'gitlab-credentials'
    ]
)

pipeline {
    agent any
    tools {
        nodejs "node"
    }

    stages {

        stage('increment version') {
            steps {
                script {
                    echo 'incrementing app version...'
                    dir('app') {
                        sh 'npm version patch'
                        def version = sh(script: 'node -p "require(\'./package.json\').version"', returnStdout: true).trim()
                        env.IMAGE_NAME = "simon808/demo-app:MOD9-${version}-$BUILD_NUMBER"
                    }
                }
            }
        }

        stage('build app') {
            steps {
                echo 'building application ...'
                buildNodeApp()
            }
        }
        stage('build image') {
            steps {
                script {
                    echo 'building the docker image...'
                    dir('app') {
                        buildImage(env.IMAGE_NAME)
                    }
                    dockerLogin()
                    dockerPush(env.IMAGE_NAME)
                }
            }
        } 
        stage("deploy to EC2") {
            steps {
                script {
                    echo 'deploying docker image to EC2...'

                    def shellCmd = "bash ./server-cmds.sh ${IMAGE_NAME}"
                    def ec2Instance = "ec2-user@54.146.93.74"
                    
                    sshagent(['ec2-server-key']) {
                       sh "scp -o StrictHostKeyChecking=no server-cmds.sh ${ec2Instance}:/home/ec2-user"
                       sh "scp -o StrictHostKeyChecking=no docker-compose.yaml ${ec2Instance}:/home/ec2-user"
                       sh "ssh -o StrictHostKeyChecking=no ${ec2Instance} ${shellCmd}"
                   }     
                }
            }
        }

        stage('commit version update'){ //2. Added the commit version stage to update the version
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: 'gitlab-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]){
                        sh 'git remote set-url origin https://$USER:$PASS@gitlab.com/twn-devops-bootcamp-nowis/aws-services-mod-09'
                        sh 'git add .'
                        sh 'git commit -m "ci: version bump"'
                        sh 'git push origin HEAD:main'
                    }
                }
            }
        }
    }
}
```

</details>

<details>
  <summary>Exercise 8: Configure access from browser (EC2 Security Group)</summary>
  
  ### Open application's port 3000 in the security group to make the app accessible from the browser:
  ccc
  aws ec2 authorize-security-group-ingress --group-id <sg-id> --protocol tcp --port 3000 --cidr 0.0.0.0/0
  ccc

  ### **Important Reminder:**
  To deploy an image on the server, ensure the server has access to Docker Hub. You need to log in to the server first before deployment:
  ccc
  ssh user@your-ec2-instance
  docker login
  ccc

  ### Screenshot:
  ![Screenshot](./images/Mod9_Exercise8.1.png)
  ![Screenshot](./images/Mod9_Exercise8.2.png)


</details>

<details>
  <summary>Exercise 9: Configure automatic triggering of multi-branch pipeline</summary>
  
  ### Add branch-based logic to Jenkinsfile:
  ```groovy
  pipeline {
      agent any
      tools {
          nodejs "node"
      }
      stages {
          stage('increment version') {
              when {
                  expression {
                      return env.GIT_BRANCH == "master"
                  }
              }
              steps {
                  script {
                      ...  
                  }
              }
          }
          stage('Run tests') {
              steps {
                  script {
                      ...  
                  }
              }
          }
          stage('Build and Push docker image') {
              when {
                  expression {
                      return env.GIT_BRANCH == "master"
                  }
              }
              steps {
                  script {
                      ...  
                  }
              }
          }
          stage('deploy to EC2') {
              when {
                  expression {
                      return env.GIT_BRANCH == "master"
                  }
              }
              steps {
                  script {
                      ...  
                  }
              }
          }
          stage('commit version update') {
              when {
                  expression {
                      return env.GIT_BRANCH == "master"
                  }
              }
              steps {
                  script {
                      ...  
                  }
              }  
          }
      }     
  }
  ```

  ### Location:
[Latest Jenksinfile](./Jenkinsfile)
[Webhooks Trigger Pipeline Instructions](./Troubleshooting_Steps/Webhooks_Trigger_Pipeline.md)

</details>
